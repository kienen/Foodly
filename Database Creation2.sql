CREATE TABLE account (
	--[variablename] DATATYPE [constraints]
	id SERIAL PRIMARY KEY --PRIMARY KEY
	, username VARCHAR(100) UNIQUE
	, password VARCHAR(100) NOT NULL
	, type INT NOT NULL
);

ALTER TABLE public.account ADD salt bytea NULL;


CREATE TABLE fridge (
	id UUID PRIMARY KEY
	, name VARCHAR(100) 
	, restaurant_id INT NOT NULL 
	, FOREIGN KEY (restaurant_id) REFERENCES account (id) ON DELETE CASCADE
);


CREATE TABLE inspector_fridge_link (
	id SERIAL PRIMARY KEY
	, inspector_id INT NOT NULL 
	, FOREIGN KEY (inspector_id) REFERENCES account (id) ON DELETE CASCADE
	, fridge_id UUID NOT NULL 
	, FOREIGN KEY (fridge_id) REFERENCES fridge (id) ON DELETE CASCADE
);

--CREATE TABLE restaurant_fridge_link (
---------
--	id SERIAL PRIMARY KEY
--	, restaurant_id UUID NOT NULL 
--	, FOREIGN KEY (restaurant_id) REFERENCES account (id) ON DELETE CASCADE
--	, fridge_id INT NOT NULL 
--	, FOREIGN KEY (fridge_id) REFERENCES fridge (id) ON DELETE CASCADE
--);

CREATE TABLE fridge_food_link (
	id SERIAL PRIMARY KEY
	, fridge_id UUID 
	, FOREIGN KEY (fridge_id) REFERENCES fridge (id) ON DELETE CASCADE
	, food VARCHAR(100) NOT
 NULL
);