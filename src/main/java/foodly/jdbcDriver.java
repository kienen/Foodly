package foodly;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.postgresql.util.PSQLException;
import java.util.UUID;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class jdbcDriver {
	private String url = "jdbc:postgresql://34.102.69.108/foodly";
	private String username = "postgres";
	private String password = "p4ssw0rd";
	private Map<Integer, Restaurant> accountCache = null; // = new HashMap<Integer, Restaurant>();
	final static Logger logjdbc = Logger.getLogger(jdbcDriver.class);

	// add account to database
	public boolean add(Account newAccount) {
		int type = 0;
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			if (newAccount instanceof Inspector) {
				type = 1;
				newAccount.setType(1);
			}

			String ourSQLStatement = "INSERT INTO account (username, password, type, salt) VALUES(?,?,?,?)";

			PreparedStatement ps = conn.prepareStatement(ourSQLStatement, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, newAccount.getUsername());
			ps.setString(2, newAccount.getPassword());
			ps.setInt(3, type);
			ps.setBytes(4, newAccount.getSalt());

			ps.executeUpdate();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				// get primary key from database and store in local object
				newAccount.setId(generatedKeys.getInt(1));
				logjdbc.debug("id " + newAccount.getId());
			} else {
				throw new PSQLException("Creating user failed, no ID obtained.", null);
			}

			return true;

		} catch (PSQLException err) {
			logjdbc.error("add account", err);
			System.out.println("This username already exists.");
			return false;
		} catch (SQLException err) {
			logjdbc.error("Add Account error", err);
			
			return false;
		}
	}

	// add fridge to database
	public boolean add(Fridge newFridge) {
		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String statement = "INSERT INTO fridge VALUES(?,?,?)";

			PreparedStatement ps = conn.prepareStatement(statement);
			ps.setObject(1, newFridge.getID());
			ps.setString(2, newFridge.getName());
			ps.setInt(3, newFridge.getOwner().getId());

			ps.executeUpdate();

			return true;

		} catch (SQLException err) {
			logjdbc.error("addFridge", err);
			System.out.println("Error Creating Fridge");
			return false;
		}
	}

	// copy account from database into local memory
	public Account findAccount(String accountName) {
		logjdbc.setLevel(Level.ERROR);
		Account thisAccount = null;

		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String ps = "SELECT * FROM account WHERE username = ?";

			PreparedStatement statement = conn.prepareStatement(ps);
			statement.setString(1, accountName);
			ResultSet results = statement.executeQuery();
			if (results.next()) {
				logjdbc.debug(results.getInt(1));
				logjdbc.debug(results.getString(2));
				logjdbc.debug(results.getString(3));
				logjdbc.debug(results.getInt(4));
				switch (results.getInt(4)) {
				case 0:
					logjdbc.trace(("r"));
					thisAccount = new Restaurant(results.getInt(1), results.getString(2), results.getString(3), 0, results.getBytes("salt"));
					break;
				case 1:
					logjdbc.trace(("i"));
					thisAccount = new Inspector(results.getInt(1), results.getString(2), results.getString(3), 1, results.getBytes("salt"));
					break;
				}
			}
		} catch (SQLException e) {
			logjdbc.error("find account ", e);
		}

		return thisAccount;
	}

	// get account id from database and copy to memory
	public int findAccountID(Account account) {
		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String ps = "SELECT * FROM account WHERE username = ?";

			PreparedStatement statement = conn.prepareStatement(ps);
			statement.setString(1, account.getUsername());
			ResultSet results = statement.executeQuery();
			if (results.next()) {
				logjdbc.debug("Returning account ID");
				logjdbc.debug(results.getInt(1));
				logjdbc.debug(results.getString(2));
				logjdbc.debug(results.getString(3));
				logjdbc.debug(results.getInt(4));
				return results.getInt(1);
			}
		} catch (SQLException err) {
			logjdbc.error("FindAccountID", err);
		}

		return -1;
	}

	// caches a map of restaurants and their primary keys
	// returns the instance of restaurant with the given primary key
	public Restaurant findRestaurantsbyID(int ID) {
		if (accountCache != null && accountCache.containsKey(ID)) {
			return accountCache.get(ID);
		}
		accountCache = new HashMap<Integer, Restaurant>();
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			String ps = "SELECT * FROM account WHERE type = 0;";

			PreparedStatement statement = conn.prepareStatement(ps);

			ResultSet results = statement.executeQuery();
			while (results.next()) {
				logjdbc.debug("Creating Restaurant");
				logjdbc.debug(results.getInt(1));
				logjdbc.debug(results.getString(2));
				logjdbc.debug(results.getString(3));
				logjdbc.debug(results.getInt(4));
				accountCache.put(results.getInt("id"), new Restaurant(results.getInt(1), results.getString(2), "", 1, results.getBytes("salt")));
			}

			// return inspectors.toArray(new Inspector[inspectors.size()]);
		} catch (SQLException e) {
			logjdbc.error("findID", e);
			logjdbc.warn("DB Error finding inspectors");
		}
		if (accountCache.containsKey(ID)) {
			return accountCache.get(ID);
		}

		logjdbc.debug("Restaurant Not Found");
		return null;
	}

	public boolean assInspector(Inspector inspector, Fridge fridge) {
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			String Statement = "INSERT INTO inspector_fridge_link (inspector_id, fridge_id) VALUES(?,?)";

			PreparedStatement ps = conn.prepareStatement(Statement);
			ps.setInt(1, inspector.getId());
			ps.setObject(2, fridge.getID());

			ps.executeUpdate();
			return true;

		} catch (SQLException err) {
			logjdbc.error("assInspector: ", err);
			System.out.println("DB Error assigning inspector");
			return false;
		}
	}

	public Inspector[] findAllInspectors() {
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			List<Inspector> inspectors = new ArrayList<>();
			String ps = "SELECT * FROM account WHERE type = 1";

			PreparedStatement statement = conn.prepareStatement(ps);

			ResultSet results = statement.executeQuery();
			while (results.next()) {
				logjdbc.debug("Creating Inspector");
				logjdbc.debug(results.getInt(1));
				logjdbc.debug(results.getString(2));
				logjdbc.debug(results.getString(3));
				logjdbc.debug(results.getInt(4));
				inspectors.add(new Inspector(results.getInt(1), results.getString(2), results.getString(3), 1, results.getBytes("salt")));
			}
			return inspectors.toArray(new Inspector[inspectors.size()]);
		} catch (SQLException e) {
			e.printStackTrace();
			logjdbc.warn("DB Error finding inspectors");
		}

		return null;
	}

	// returns an array containing all fridges this restaurant has access to
	public Fridge[] findFridges(Restaurant owner) {
		Map<UUID, Fridge> fridges = new HashMap<>();

		if (owner.getId() < 0) {
			owner.setId(0);
		}
		if (owner.getId() < 0) {
			System.out.println("Restaurant not found in DB");
			return null;
		}

		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			// left join returns all tables owned by this restaurant
			//  and all of the food inside.
			String command = "SELECT * FROM fridge LEFT JOIN fridge_food_link "
					+ "ON fridge.id = fridge_food_link.fridge_id "
					+ "WHERE fridge.restaurant_id =  ? ORDER BY fridge_id;";

			PreparedStatement statement = conn.prepareStatement(command);
			statement.setInt(1, owner.getId());
			ResultSet results = statement.executeQuery();

			while (results.next()) {
				UUID id = (java.util.UUID) results.getObject("id");
				logjdbc.debug("fridge " + id);
				if (fridges.containsKey(id)) {

					logjdbc.debug("Fridge exists");
					if (results.getString("food") != null) {
						fridges.get(id).addFood(results.getString("food"));
					}
					logjdbc.debug("283 " + results.getString("food"));

				} else {
					logjdbc.trace("create new fridge");
					logjdbc.debug("287 " + results.getString("food"));
					Fridge newFridge = new Fridge(owner, results.getString("name"), id);
					fridges.put(id, newFridge);
					if (results.getString("food") != null) {
						logjdbc.debug(results.getString("food") +" in " + newFridge.getName());
						newFridge.getFoods().add(results.getString("food"));
					}
				}

			}

			Collection<Fridge> values = fridges.values();
			return values.toArray(new Fridge[0]);
		} catch (SQLException e) {
			logjdbc.error("Find Fridges", e);

		}

		return null;
	}

	// returns an array containing all fridges this restaurant has access to
	public Fridge[] findFridges(Inspector inspector) {

		Map<UUID, Fridge> fridges = new HashMap<>();

		if (inspector.getId() < 0) {
			inspector.setId(0);
		}
		if (inspector.getId() < 0) {
			System.out.println("Inspector not found in DB");
			return null;
		}
		if (accountCache == null) {
			findRestaurantsbyID(-1);
		}

		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			// left join returns a list of all fridges and all food inside
			String command = "SELECT * FROM fridge LEFT JOIN fridge_food_link "
					+ "ON fridge.id = fridge_food_link.fridge_id " + "WHERE fridge.id IN "
					+ "(SELECT fridge_id FROM inspector_fridge_link WHERE inspector_id = ?)  ;";

			PreparedStatement statement = conn.prepareStatement(command);
			statement.setInt(1, inspector.getId());
			ResultSet results = statement.executeQuery();

			while (results.next()) {
				UUID id = (java.util.UUID) results.getObject("fridge_id");
				Restaurant owner = findRestaurantsbyID(results.getInt("restaurant_id"));
				if (fridges.containsKey(id)) {
					if (results.getString("food") != null) {
						fridges.get(id).addFood(results.getString("food"));
					}
				} else {
					Fridge newFridge = new Fridge(owner, results.getString("name"), id);
					fridges.put(id, newFridge);
					if (results.getString("food") != null) {
						newFridge.getFoods().add(results.getString("food"));
					}
				}

			}

			Collection<Fridge> values = fridges.values();
			return values.toArray(new Fridge[0]);
		} catch (SQLException e) {
			logjdbc.error("Find Fridges (Inspector)", e);

		}

		return null;
	}

	// store food in database
	public boolean addFood(Fridge fridge, String food) {
		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String statement = "INSERT INTO fridge_food_link (fridge_id, food) VALUES(?,?)";

			PreparedStatement ps = conn.prepareStatement(statement);
			ps.setObject(1, fridge.getID());
			ps.setString(2, food);

			ps.executeUpdate();

			return true;

		} catch (SQLException err) {
			logjdbc.error(err);
			System.out.println("Error Adding Food");
			return false;
		}
	}

	// copy fridge from database into memory
	public boolean updateFridge(Fridge fridge) {
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			String command = "SELECT * FROM fridge_food_link WHERE fridge_id = ?";

			PreparedStatement statement = conn.prepareStatement(command);
			statement.setObject(1, fridge.getID());
			ResultSet results = statement.executeQuery();

			List<String> newFoods = new ArrayList<>();

			while (results.next()) {
				logjdbc.trace("food " + results.getString(0));
				newFoods.add(results.getString(0));

			}
			fridge.setFoods(newFoods);
			return true;
		} catch (SQLException e) {
			logjdbc.warn("update fridge", e);
			System.out.println("DB Error in update fridge");
		}

		return false;
	}

	// remove fridge from database
	public boolean remove(Fridge fridge) {
		try (Connection conn = DriverManager.getConnection(url, username, password)) {

			String statement = "DELETE FROM fridge WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(statement);
			ps.setObject(1, fridge.getID());

			ps.executeUpdate();
			System.out.println("Fridge:" + fridge.getName() + " has been deleted.");
			return true;

		} catch (SQLException err) {
			logjdbc.error("removeFridge", err);
			System.out.println("Error DELETING Fridge");
			return false;
		}

	}

	// remove food from database
	public boolean removeFood(Fridge fridge, String food) {
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			logjdbc.trace("406");
			String statement = "DELETE FROM fridge_food_link WHERE food = ? AND fridge_id = ?";

			PreparedStatement ps = conn.prepareStatement(statement);
			ps.setString(1, food);
			ps.setObject(2, fridge.getID());

			ps.executeUpdate();
			System.out.println(food + " has been removed from " + fridge.getName() + ".");
			return true;

		} catch (SQLException err) {
			logjdbc.error("removeFridge", err);
			System.out.println("Error DELETING Fridge");
			return false;
		}
	}

	public static void main(String[] args) {
		//logjdbc.setLevel(Level.ALL);
		FoodlyMain.main(args);
	}
}