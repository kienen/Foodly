package foodly;

import java.util.Scanner;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class FoodlyMain {
	final static Logger logMain = Logger.getLogger(FoodlyMain.class);

	// Lists Food available in fridge and takes user input to select.
	// Returns null if unsuccessful.
	public static String foodSelector(Fridge fridge, String... varPrompts) {
		if (fridge.isEmpty()) {
			System.out.println("This refigerator (" + fridge.getName() + ") is empty");
			return null;
		}

		String[] foods = fridge.getFoods().toArray(new String[0]);
		String[] prompts = new String[foods.length + varPrompts.length];
		int fNum = 0;
		for (String food : fridge.getFoods()) {
			prompts[fNum] = fNum + ": " + food;
			fNum++;
		}
		for (String str : varPrompts) {
			prompts[fNum] = str;
			fNum++;
		}

		int choice = getUserChoice(prompts);
		System.out.println(foods[choice] + " selected");
		return foods[choice];
	}

	// Selects fridge from fridges available to inspector passed by argument
	// returns null if unsuccessful
	// Overloaded. This takes an Inspector parameter
	public static Fridge fridgeSelector(Inspector user, String... varPrompts) {
		Fridge[] myFridges = FoodlyState.getMyFridges(user);

		switch (myFridges.length) {
		case 0:
			System.out.println("No assigned Fridges.");
			return null;
		case 1:
			return myFridges[0];
		case 2:
		}
		int fNum = 0;
		String[] prompt = new String[myFridges.length + varPrompts.length];

		for (Fridge fridge : myFridges) {
			prompt[fNum] = fNum + ": " + fridge.getName();
			fNum++;
		}
		for (String str : varPrompts) {
			prompt[fNum] = str;
			fNum++;
		}

		int choice = getUserChoice(prompt);

		System.out.println(myFridges[choice].getName() + " selected");
		return myFridges[choice];

	}

	// Selects Fridge from Fridges available to Restaurant passed as an argument
	// returns null if unsuccessful
	// Overloaded. This takes Restaurant parameter.
	public static Fridge fridgeSelector(Restaurant owner, String... varPrompts) {
		Fridge[] myFridges = FoodlyState.getMyFridges(owner);

		switch (myFridges.length) {
		case 0:
			System.out.println("Please buy a refrigerator first");
			return null;
		case 1:
			return myFridges[0];
		case 2:
		}
		int fNum = 0;
		String[] prompt = new String[myFridges.length + varPrompts.length];

		for (Fridge fridge : myFridges) {
			prompt[fNum] = fNum + ": " + fridge.getName();
			fNum++;
		}
		for (String str : varPrompts) {
			prompt[fNum] = str;
			fNum++;
		}
		
		int choice = 1000;
		do {
			choice = getUserChoice(prompt);
		} while (choice >= myFridges.length);
		return myFridges[choice];

	}

	// Accepts user prompts passed by argument and returns a single integer.
	public static int getUserChoice(String... prompts) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		int input;
		System.out.println("---");
		for (String str : prompts) {
			System.out.println(str);
		}
		while (true) {
			try {
				input = Integer.parseInt(sc.next());
				break;
			} catch (NumberFormatException err) {
				System.out.println("This input must be a number.");
			}

		}
		return input;
	}

	// Accepts prompts passed by argument and returns a String.
	public static String getUserInput(String... prompt) {
		String input;
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("---");
		for (String str : prompt) {
			System.out.println(str);
		}
		System.out.println();
		input = sc.nextLine();

		return input;

	}

	// Main menu for Inspector accounts
	public static Account inspectorMenu(Inspector user) {
		Fridge[] myFridges = FoodlyState.getMyFridges(user);

		int choice = getUserChoice("1. View Fridges", "2. Remove Food", "3. Logout");

		switch (choice) {
		case 1:
			if (myFridges.length == 0) {
				System.out.println("No Assigned Fridges");
				break;
			}
			for (Fridge fridge : myFridges) {
				System.out.println(fridge.pretty());
			}
			break;
		case 2:
			Fridge fridge = fridgeSelector(user, "Which Fridge?");
			if (fridge == null) {
				break;
			}
			String food = foodSelector(fridge, "What would you like to remove?");
			if (food == null) {
				break;
			}
			if (FoodlyState.removeFood(fridge, food)) {
				System.out.println("Removed: " + food);
			}
			break;
		case 3:
			return null;
		case 4:
			System.out.println("Please enter a number between 1-3.");
		}
		return user;
	}

	// Lists Inspectors as a submenu and allows user to choose which object to
	// return.
	public static Inspector inspectorSelector(String... varPrompts) {
		Inspector[] inspectors = FoodlyState.getInspectors();

		if (inspectors.length == 0) {
			System.out.println("No available Health Inspectors.");
			return null;
		}
		String[] prompts = new String[inspectors.length + varPrompts.length];
		int iNum = 0;
		for (Inspector thisInspector : inspectors) {
			prompts[iNum] = iNum + ": " + thisInspector.getUsername();
			iNum++;
		}
		for (String str : varPrompts) {
			prompts[iNum] = str;
			iNum++;
		}
		int choice = getUserChoice(prompts);

		return inspectors[choice];
	}

	// Login authenticator
	static Account login(String login, String password) {
		Account user = FoodlyState.findAccountByUsername(login);
		logMain.debug("main " + password);
		if (user != null && user.checkPassword(password)) {
			return user;
		}
		System.out.println("Invalid username or password");
		return null;
	}

	// Login menu
	public static Account loginMenu() {
		// Home Menu
		String login;
		String password;
		Account user = null;
		while (user == null) {
			int homeMenu = getUserChoice("Login Menu:", "1. Login", "2. Register", "3. Exit");
			switch (homeMenu) {
			case 1:
				login = getUserInput("Login: ");
				password = getUserInput("Password: "); // encrypt(getUserInput ("Password: "));
				if (login.equals("") || password.equals("")) {
					System.out.println("Invalid username or password.");
					continue;
				}
				user = login(login, password);
				break;
			case 2:
				login = getUserInput("Login: ");
				password = getUserInput("Password: "); // encrypt(getUserInput ("Password: "));
				if (login.equals("") || password.equals("")) {
					System.out.println("Invalid username or password.");
					continue;
				}
				user = register(login, password);
				break;
			case 3:
				return null;
			case 4:
				System.out.println("Please enter a number from 1-3.");
			}
		}
		return user;
	}

	// Main
	// Application Event loop calls loginMenu to authenticate users
	// When user is found, it sends the user to the appropriate menu, then back to
	// login menu upon logout.
	public static void main(String[] args) {
		//logMain.setLevel(Level.ALL);
		logMain.setLevel(Level.ERROR);
		Account user = loginMenu();

		while (user != null) {
			//
			while (user != null && user instanceof Restaurant) {
				System.out.println("---");
				System.out.println("Welcome: " + user.getUsername());
				user = restaurantMenu((Restaurant) user);
				// FoodlyState.persistAllData();
			}
			while (user != null && user instanceof Inspector) {
				System.out.println("---");
				System.out.println("Welcome: " + user.getUsername());
				user = inspectorMenu((Inspector) user);
			}
			user = loginMenu();
		}
	}

	// Submenu to create a new account.
	static Account register(String login, String password) {
		Account newAccount = null;
		int type = getUserChoice("1. Restaurant", "2. Health Inspector");

		switch (type) {
		case 1: // Restaurant
			newAccount = FoodlyState.newRestaurant(login, password);
			break;
		case 2: // Inspector
			newAccount = FoodlyState.newInspector(login, password);
		}
		return newAccount;
	}

	// Main menu for Restaurant users
	public static Account restaurantMenu(Restaurant user) {
		Fridge fridge;
		Fridge destFridge;
		String food;

		int choice = getUserChoice("1. Add Food", "2. Remove Food", "3. Transfer Food Between Fridges",
				"4. Buy New Fridge", "5. Assign Health Inspector", "6. Inventory", "7. Remove Refrigerator",
				"8. Logout");
		switch (choice) {
		case 1: // Add Food
			fridge = fridgeSelector(user, "Which Fridge?");
			if (fridge == null) {
				break;
			}
			food = getUserInput("What sort of food?");
			if (FoodlyState.addFood(fridge, food)) {
				System.out.println("Stored: " + food + " in " + fridge.getName());
			}
			break;
		case 2: // Remove food
			fridge = fridgeSelector(user, "Which Fridge?");
			if (fridge == null) {
				break;
			}
			// System.out.println(fridge.pretty());
			food = foodSelector(fridge, "What would you like to remove?");
			if (food == null) {
				break;
			}
			FoodlyState.removeFood(fridge, food);
			break;
		case 3: // "3. Transfer Food Between Fridges"
			fridge = fridgeSelector(user, "Which Fridge are you taking food out of?");
			if (fridge == null) {
				logMain.trace("fridge = null");
				break;
			}
			if (fridge.isEmpty()) {
				logMain.trace("fridge is empty");
				System.out.println(fridge.getName() + " is empty.");
				break;
			}
			destFridge = fridgeSelector(user, "Which Fridge is the food going into?");
			if (destFridge.spaceLeft() == 0) {
				logMain.trace("dest is full");
				System.out.println(destFridge.getName() + " is full.");
				break;
			}
			if (fridge == destFridge) {
				System.out.println(
						"You move the food to another shelf in the same fridge. You feel strangely fulfilled.");
				break;
			}
			food = foodSelector(fridge, "What would you like to transfer?");
			FoodlyState.removeFood(fridge, food);
			FoodlyState.addFood(destFridge, food);
			System.out.println("Transferred " + food + " from " + fridge.getName() + " to " + destFridge.getName());
			break;
		case 4: // New Fridge
			String fridgeName = getUserInput("Name your new refrigerator: ");
			FoodlyState.newFridge(user, fridgeName);
			break;
		case 5: // assign inspector
			Inspector inspector = inspectorSelector("Which Inspector?");
			if (inspector == null) {
				break;
			}
			fridge = fridgeSelector(user, "Which fridge?");
			if (fridge == null) {
				break;
			}
			FoodlyState.assInspector(inspector, fridge);
			break;
		case 6: // INVENTORY
			Fridge[] myFridges = FoodlyState.getMyFridges(user);
			for (Fridge thisFridge : myFridges) {
				System.out.println(thisFridge.pretty());
			}
			break;
		case 7: // Remove fridge
			Fridge rmFridge = fridgeSelector(user, "Which fridge would you like to remove?");
			if (rmFridge == null) {
				break;
			}
			Fridge[] myFridges2 = FoodlyState.getMyFridges(user);
			int spaceAvailable = 0;
			for (Fridge thisFridge : myFridges2) {
				spaceAvailable += thisFridge.spaceLeft();
			}
			spaceAvailable -= rmFridge.spaceLeft();
			String[] foods = rmFridge.getFoods().toArray(new String[0]);
			int confirm = 1;
			if (spaceAvailable < foods.length) {
				String[] prompts = new String[3 + foods.length - spaceAvailable];
				prompts[0] = "*WARNING: These foods will be discarded*";
				int prmpt = 1;
				for (int i = spaceAvailable; i < foods.length; i++) {
					prompts[prmpt] = foods[i];
					prmpt++;
				}
				prompts[prmpt] = "0: No, I do not want to discard this food. No changes will be made.";
				prompts[prmpt + 1] = "1: I confirm that I want to discard this food.";
				confirm = getUserChoice(prompts);
			}
			if (confirm == 1 && foods.length > 0) {
				int trIdx = 0;
				FridgeLoop: for (Fridge destFridge2 : myFridges2) {
					if (destFridge2 == rmFridge) {
						continue FridgeLoop;
					}
					while (destFridge2.spaceLeft() > 0) {
						FoodlyState.addFood(destFridge2, foods[trIdx]);
						FoodlyState.removeFood(rmFridge, foods[trIdx]);
						System.out.println(foods[trIdx] + " has been placed in " + destFridge2.getName());
						trIdx++;
						if (trIdx == foods.length) {
							System.out.println("Food has been safely stored;");
							break FridgeLoop;
						}
					}
				}
				if (FoodlyState.removeFridge(rmFridge)) {
					for (String disFd : rmFridge.getFoods()) {
						System.out.println(disFd + " has been discarded.");
					}
					System.out.println(rmFridge.getName() + " has been destroyed.");
				} else {
					System.out.println("An error has occurred.");
				}
			}
			break;
		case 8: // Logout
			return null;
		case 9:
			System.out.println("Please enter a number between 1-8.");
		}

		return user;
	}

}
