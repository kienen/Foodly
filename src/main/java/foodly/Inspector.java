package foodly;

import java.util.ArrayList;
import java.util.List;

public class Inspector extends Account {
	private List <Fridge> fridges = new ArrayList<> ();

	//Constructor takes 2 strings as parameters.
	//Accounts are not allowed to exist without username and password.
	public Inspector (String username, String password) {
		super(username, password);
	}
	
	public Inspector (int id, String username, String password, int type, byte [] salt) {
		super (username, password, salt);
		this.setId(id); 
		this.setType(type);
	}

	//Assigns Fridge to Inspector
	public void assFridge(Fridge fridge) {
		if (!fridges.contains(fridge) ) {
			fridges.add(fridge);
		}
	}

//	//Returns available fridges as an array
//	public Fridge []  getFridges() {
//		return fridges.toArray(new Fridge [fridges.size()]);
//	}
//
//	//Removes fridge from List Inspector has permissions for.
//	public void rmFridge(Fridge fridge) {
//		fridges.remove(fridge);
//	}

	//overrides toString from Accounts, designed to save Application state to disk
	@Override
	public String toString () {
		String str;

		str = this.getUsername() + ",";
		str += this.getPassword();
		for (Fridge fridge : fridges) {
			str += "," + fridge.getName();
		}

		return str;

	}
}
