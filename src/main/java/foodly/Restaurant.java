package foodly;

public class Restaurant extends Account {
	
	//Constructor takes 2 strings as parameters 
	//Accounts are not allowed to exist without a username and password
	public Restaurant (String username, String password) {
		super(username, password);
	}	
	
	public Restaurant (int id, String username, String password, int type, byte [] salt) {
		super (username, password, salt);
		this.setId(id); 
		this.setType(type);
	}
}

