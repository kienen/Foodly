package foodly;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Fridge {
	//Change the SIZE constant in order to change capacity.
	private final int SIZE = 3;
	public int getSIZE() {
		return SIZE;
	}

	private List <String> foods = new ArrayList <> ();
	
	public void setFoods(List <String> foods) {
		// foods  = new ArrayList <> ();
		this.foods = foods;
	}

	private String name;
	private UUID uniqueID;
	private Restaurant owner;

	//Overloaded constructor so Fridge can be created without a name
	Fridge (Restaurant owner) {
		this.uniqueID = UUID.randomUUID();
		this.owner = owner;
	}

	//Overloaded constructor so Fridge can be created with a name.
	Fridge (Restaurant owner, String name) {
		this.uniqueID = UUID.randomUUID();
		this.owner = owner;
		this.name = name;
	}
	
	
	Fridge (Restaurant owner, String name, List <String> foods) {
		this.uniqueID = UUID.randomUUID();
		this.owner = owner;
		this.name = name;
		this.foods = foods;
	}
	
	//Constructor to populate Fridges from the database
	Fridge (Restaurant owner, String name, UUID uuid) {
		this.owner = owner;
		this.name = name;
		this.uniqueID = uuid;
	}

	//Add Food. Returns false if unsuccessful
	protected boolean addFood (String thisFood) {
		if (foods.size() < SIZE) {
			foods.add(thisFood);
			return true;
		}

		System.out.println("Fridge is full.");
		return false;
	}

	//Allow List of foods to be modified
	public List<String> getFoods() {
		return foods;
	}

	//ID getter
	public UUID getID() {
		return uniqueID;
	}

	//Name getter
	public String getName() {
		return name;
	}

	//Owner getter
	public Restaurant getOwner() {
		return owner;
	}

	//Returns true if Fridge has no food. 
	public boolean isEmpty () {
		if (foods.size() == 0) {
			return true;
		}
		return false;
	}

	//Human readable representation of the class
	public String pretty () {
		String str =  "Owner: " + owner.getUsername() + "\n" +
				"Name: " + name + "\n" +
				"Contents: " ;

		for (String food : foods) {
			if (food != null) { str+= food + ", "; }
		}

		//str += "\n";
		return str;
	}

	//remove food
	protected boolean removeFood (String thisFood) {
		boolean success = foods.remove(thisFood);
		return success;
	}

	//name getter
	public void setName(String name) {
		this.name = name;
	}

	//returns space available
	public int spaceLeft () {
		return SIZE - foods.size();
	}

	//designed to save Application state to disk
	public String toString () {
		String str = owner.getUsername() + "," + name;
		for (String food : foods) {
			if (food != null) {str+= "," + food;}
		}
		return str;
	}

	public static void main(String[] args) {
		FoodlyMain.main(args);
	}
}
