package foodly;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

abstract class Account {
	private int id = -1;
	private String username;
	private String password;
	private int type = 0;
	private byte[] salt = new byte[16];
	final static Logger log = Logger.getLogger(Account.class);
	
	
	
	public byte[] getSalt() {
		return salt;
	}

	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getId() {
		if (id == -1) {
			id = FoodlyState.findAccountID(this);
		}
		return id;
	}

	public void setId(int id) {
		if (id > 0 ) {
			this.id = id;
		} else {
			int databaseID =  FoodlyState.findAccountID(this);
			if (databaseID == -1) {
				System.out.println("DB Error: Account not found");
			}
			this.id = databaseID;
		}
	}

	public Account (String username, String password, byte [] salt) {
		this.username = username;
		this.password = password;
		this.salt = salt;
	}
	
	public Account (String username, String password) {
		boolean salted = false;
		this.username = username;
		this.password = password;
		for (byte i : salt) {
			if (i != 0) {
				salted =true;
			}
		}
		if (!salted) {
			this.salt = makeSalt();
			this.password = encrypt(password);
			log.debug(this.password);
		}
	}
	
	public Account (String username, int id) {
		this.username = username;
		this.id = id;
	}

	public boolean checkPassword (String str) {
		String cryptostr = encrypt (str);
		log.debug("login" + password + " " + cryptostr);
		log.debug(password);
		if (password.equals(cryptostr)) {
			return true;
		}
		return false;
	}
	
	public String getPassword() {
		return password;
	}
	public String getUsername() {
		return username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String toString() {
		return username + "," + password; 
	}
	
	public String encrypt(String passwordToHash) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(salt);
			byte[] bytes = md.digest(passwordToHash.getBytes());
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			log.debug("encrypted password " +passwordToHash +" "+ sb.toString());
			return sb.toString();

		} catch (NoSuchAlgorithmException e) {
			System.out.println("Encryption disabled.");
			return passwordToHash;
		}

	}

	private static byte[] makeSalt() {
		SecureRandom sr = new SecureRandom();
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		log.debug(salt);
		return salt;
	}
	
	public static void main(String[] args) {
		//log.setLevel(Level.ALL);
		
		FoodlyMain.main(args);
	}
}
