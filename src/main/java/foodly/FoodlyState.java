package foodly;

import java.util.ArrayList;
import java.util.List;

public class FoodlyState {
	private static jdbcDriver Database = new jdbcDriver();

	// Assign Inspector to Fridge
	public static void assInspector(Inspector inspector, Fridge fridge) {
		// inspector.assFridge(fridge);
		if (Database.assInspector(inspector, fridge)) {
			System.out.println(inspector.getUsername() + " has been given access to " + fridge.getName() + ".");
		}
	}

	// Find Account by String.
	public static Account findAccountByUsername(String username) {
		return Database.findAccount(username);
	}



	public static int findAccountID(Account account) {
		return Database.findAccountID(account);
	}


	// Returns array on Inspectors. 
	public static Inspector[] getInspectors() {
		return Database.findAllInspectors();
	}

	// Returns list of Fridges owned by this Restaurant
	public static Fridge[] getMyFridges(Restaurant owner) {
		return Database.findFridges(owner);
	}

	// Creates a new Fridge and adds to fridges list
	public static void newFridge(Restaurant owner, String name) {
		// fridges.add(new Fridge (owner, name));
		List<String> foods = new ArrayList<>();
		Database.add(new Fridge(owner, name, foods));
	}

	// Creates a new Inspector account
	public static Inspector newInspector(String login, String password) {
		Inspector newInspector = new Inspector(login, password);
		if (Database.add(newInspector)) {
			return newInspector;
		}

		return null;
	}

	// Creates a new Restaurant account
	public static Restaurant newRestaurant(String login, String password) {
		Restaurant newRestaurant = new Restaurant(login, password);
		if (Database.add(newRestaurant)) {
			return newRestaurant;
		}
		return null;
	}

	// Return fridges assigned
	public static Fridge[] getMyFridges(Inspector inspector) {
		return Database.findFridges(inspector);
	}

	// Remove Fridge from Database
	public static boolean removeFridge(Fridge fridge) {
		return Database.remove(fridge);
	}

	public static boolean addFood(Fridge fridge, String food) {
		if (fridge.addFood(food)) {
			Database.addFood(fridge, food);
			return true;
		}
		return false;
	}

	public static boolean removeFood(Fridge fridge, String food) {
		if (fridge.removeFood(food)) {
			Database.removeFood(fridge, food);
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		FoodlyMain.main(args);
	}
}
