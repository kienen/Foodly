## FOODLY
---
A Java application to organize and track stored food.
---
# Technologies Used
    Java
    SQL
    PostgreSQL
---
# Features

Add/Place Food into a Fridge
Remove Fridges at any time
Change size of Fridges easily
Transfer Food between Fridges
Persist data in Postgres database
Logging in Log4J
---
# Getting Started
```
git clone https://gitlab.com/kienen/Foodly.git
java target\classes\foodly\FoodlyMain
```

